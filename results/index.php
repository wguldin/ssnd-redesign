<?php include('../perch/runtime.php');?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php perch_layout('global.head'); ?>
    <?php perch_page_attributes(array('template' => 'seo.html' )); ?> 
</head>

<body>
    <?php perch_layout('global.header'); ?>

    <main role="main" class="cyan-border contest">
      <article class="results">
          <h1 class="page-header">Archived Contest Results</h1>
          <?php 
              perch_pages_navigation(array(
                  'navgroup' => 'results',
                  'template' => 'item-overview.html'
              ));
          ?>
      </article>
    </main>

    <?php perch_layout('global.footer'); ?>

  </body>
</html>