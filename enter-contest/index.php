<?php include('../perch/runtime.php');?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php perch_layout('global.head'); ?>
    <?php perch_page_attributes(array('template' => 'seo.html' )); ?> 
</head>

<body>
    <?php perch_layout('global.header'); ?>

    <main role="main" class="magenta-border contest">
      <article class="contest">
        <div>
          <h1 class="page-header">2014 College News Design Contest</h1>
          <em>The Missouri Student Society of News Design hosts a juried contest to honor excellence in design and graphics. The contest is co-sponsored by the <a href="http://www.snd.org/">Society for News Design.</a></em>
          <?php perch_content('entry-form'); ?>
        </div>
        
        <div class="contest-details">
          <section class="contest-requirements">
            <?php perch_content('contest-eligability'); ?>
            <?php perch_content('contest-how-to'); ?>
            <?php perch_content('contest-contact'); ?>

          </section>
          <section class="contest-requirements">
            <?php perch_content('contest-instructions'); ?>
          </section>
        </div>

        <div class="contest-details">

          <section class="contest-categories">
            <?php perch_content('contest-category-a'); ?>
          </section>

          <section class="contest-categories">
            <?php perch_content('contest-category-b'); ?>
            <?php perch_content('contest-category-c'); ?>
          </section>

        </div>
      </article>
    </main>

    <?php perch_layout('global.footer'); ?>

  </body>
</html>