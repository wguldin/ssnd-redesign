<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php perch_layout('global.head'); ?>
    <?php perch_page_attributes(array('template' => 'seo.html' )); ?> 
</head>

<body>
	<?php perch_layout('global.header'); ?>

    <main role="main" class="yellow-border blog">
        <article class="post">
        	<?php perch_blog_post(perch_get('s')); ?>

        	<?php perch_blog_author_for_post(perch_get('s')); ?>
        	
        	<?php perch_blog_post_comments(perch_get('s')); ?>
        	
        	<?php perch_blog_post_comment_form(perch_get('s')); ?>
            
        </article>

		<nav class="sidebar">
		    <?php perch_blog_date_archive_months(); ?>
        </nav>
   </main>
	<?php perch_layout('global.footer'); ?>
</body>
</html>