<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php perch_layout('global.head'); ?>
    <?php perch_page_attributes(array('template' => 'seo.html' )); ?> 
</head>

<body>
		<?php perch_layout('global.header'); ?>
	
		<main role="main" class="yellow-border blog">
		   
		   <?php perch_blog_authors(); ?>
		
			<nav class="sidebar">
			    <h2>Archive</h2>
			    <!-- The following functions are different ways to display archives. You can use any or all of these. 
			    
			    All of these functions can take a parameter of a template to overwrite the default template, for example:
			    
			    perch_blog_categories('my_template.html');
			    
			    --> 
			    <!--  By category listing -->
			    <?php perch_blog_categories(); ?>
			    <!--  By tag -->
			    <?php perch_blog_tags(); ?>
			    <!--  By year -->
			    <?php perch_blog_date_archive_years(); ?>
			    <!--  By year and then month - can take parameters for two templates. The first displays the years and the second the months see the default templates for examples -->
			    <?php perch_blog_date_archive_months(); ?>
	    	</nav>
		</main>
	
	  <?php perch_layout('global.footer'); ?>
</body>
</html>