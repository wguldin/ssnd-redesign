<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
			<title>Sections - Blog Example</title>
	    <meta name="description" content="Missouri Chapter of the Student Society of News Design">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <link rel="stylesheet" href="/css/style.css">
	    <link rel="stylesheet" href="/css/blog.css" type="text/css" />
			<link rel="alternate" type="application/rss+xml" title="RSS" href="rss.php" />

	    <!--[if lt IE 9]>
	        <link rel="stylesheet" type="text/css" href="ie.css" />
	    <![endif]-->

	    <script src="js/modernizr-html5shiv.min.js"></script>
	</head>
	<body>
		
		<?php perch_layout('global.header'); ?>

			<main role="main" class="yellow-border blog">
			    <article class="post">
			    	<?php perch_blog_sections(); ?>
			    </article>
			</main>
		</div>

		<?php perch_layout('global.footer'); ?>
	</body>
</html>