<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php perch_layout('global.head'); ?>
    <?php perch_page_attributes(array('template' => 'seo.html' )); ?> 
</head>

<body>
	<?php perch_layout('global.header'); ?>

	<main role="main" class="yellow-border blog">
		    <article class="post">
		    	<?php perch_blog_section(perch_get('s')); ?>

		    	<?php perch_blog_custom([
		    		'section'=>perch_get('s'),
		    	]); ?>
		    </article>
		</div>
		
		<nav class="sidebar">
		    <h2>Filter archive</h2>
		    <!--  By category listing -->
		    <?php perch_blog_categories(array('section'=>perch_get('s'))); ?>
		    <!--  By tag -->
		    <?php perch_blog_tags(array('section'=>perch_get('s'))); ?>
		    <!--  By year -->
		    <?php perch_blog_date_archive_years(array('section'=>perch_get('s'))); ?>
    	</nav>
	</div>
	
	<?php perch_layout('global.footer'); ?>
</body>
</html>