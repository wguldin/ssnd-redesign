<?php include('../perch/runtime.php'); ?>

<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
    <?php perch_layout('global.head'); ?>
    <?php perch_page_attributes(array('template' => 'seo.html' )); ?> 
</head>

<body>
	<?php perch_layout('global.header'); ?>

		<main role="main" class="yellow-border blog">
			<article class="post-listing">
			  <h1 class="page-header">SSND Missouri Blog</h1>
			  <em>The live blog for the Missouri Student Society of News Design. Each year, we host the national College News Design Contest.</em>
				<!-- this is an example blog homepage showing a simple call to perch_blog_recent_posts()
				
				Posts are displayed using the templates stored in perch/apps/perch_blog/templates/blog you can edit these as you wish, making sure that the 
				paths used in these templates are correct for your installation.
				 -->
			    <?php 
			        perch_blog_recent_posts(7);
			    ?>
			</article>
			<nav class="sidebar">
			    <!-- The following functions are different ways to display archives. You can use any or all of these. 
			    
			    All of these functions can take a parameter of a template to overwrite the default template, for example:
			    
			    perch_blog_categories('my_template.html');
			    
			    --> 
			    <!--  By year and then month - can take parameters for two templates. The first displays the years and the second the months see the default templates for examples -->
			    <?php perch_blog_date_archive_months(); ?>
			</nav>

		</main>

	
  <?php perch_layout('global.footer'); ?>
</body>
</html>